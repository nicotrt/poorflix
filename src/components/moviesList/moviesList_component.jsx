import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';

class MoviesList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            filtered: []
        }
    }

    /* componentDidMount() {
        this.setState({
          filtered: this.props.items
        });
      }
      
      componentWillReceiveProps(nextProps) {
        this.setState({
          filtered: nextProps.items
        });
      } */

    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== this.state.data) {
            this.setState({data: nextProps.data});
        }
        console.log('data was successfully updated');
    }

    addMovieToFavList(movieToAdd) {
        this.props.callbackFromParent(movieToAdd);
    }

    render() {
        let renderMovies = '';
        if (this.state.data !== undefined && this.state.data.length !== 0) {
            renderMovies = this.state.data.map((movie) => {
                return (
                    <Row className="movItem">
                        <Col sm={3}>
                            <img src={"http://image.tmdb.org/t/p/w185//" + movie.poster_path} />
                        </Col>
                        <Col sm={9}>
                            <h4>{movie.title}</h4>
                            <Button className="adToFavList" onClick={this.addMovieToFavList.bind(this, movie)}>
                                Add to Fav
                            </Button>
                        </Col>
                        
                    </Row>
                );
            });
        }
            return (
                <div className="Movies">
                    {renderMovies}
                </div>
            );
        }
}

export default MoviesList;