import React from 'react';
import {Row, Col} from 'react-bootstrap';

class Header extends React.Component {
    render() {
        return (
            <Row>
                <Col>
                    <h1>Welcome to PoorFlix!</h1>
                </Col>
            </Row>
            
        );  
    }
}

export default Header;