import React from 'react';
import {Row, Col} from 'react-bootstrap';

class Footer extends React.Component {
    render() {
        return (
            <Row>
                <Col>
                    <p>Coded by nko</p>
                    <p>Powered by React and Node.js</p>
                </Col>
            </Row>
        );  
    }
}

export default Footer;