import React from 'react';
import {Row, Col} from 'react-bootstrap';

class Favorites extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            movies: []
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.movie && nextProps.movie !== this.state.movie) {
            let alreradyExistInList = false;
            this.state.movies.forEach(movie => {
                if(movie.title === nextProps.movie.title) {
                    alreradyExistInList = true;
                }
            });
            if (!alreradyExistInList) {
                this.state.movies.push(nextProps.movie);
            }
        }
    }

    deleteFromList(movieTitle) {
        let newMovieList = [];
        this.state.movies.forEach(movie => {
            if(movie.title !== movieTitle) {
                newMovieList.push(movie);
            }
        });
        this.setState({ movies: newMovieList });
    }

    render() {
        let renderMovies = "";
        if (this.state.movies !== undefined && this.state.movies.length > 0) {
            renderMovies = this.state.movies.map(movie =>{
                console.log(movie, ': successfully added to FavList');
                return (
                    <Row className="FavItems">
                        <Col sm={11}>
                            <p>{movie.title}</p>
                        </Col>
                        <Col sm={1}>
                            <button className="deleteFromList" onClick={this.deleteFromList.bind(this, movie.title)}>Delete from FavList</button>
                        </Col>
                    </Row>  
                );
            });
        }
        return (
            <div className="FavList">
                <p>Favorites</p>
                {renderMovies}
            </div>
        );
    }
}

export default Favorites;