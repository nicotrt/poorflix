import React from 'react';
/* import imdb from 'imdb-api' */
import { InputGroup, FormControl } from 'react-bootstrap';

class SearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            searchInput: ''
        }

        this.handleSearch = this.handleSearch.bind(this);
    }
    render() {
        return (
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Recherche</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl onChange={this.handleSearch} placeholder="Find your movie..."/>
            </InputGroup>
        );  
    }

    handleSearch(e) {
        this.setState({searchInput: e.target.value});
        console.log(e.target.value);
        fetch('https://api.themoviedb.org/3/search/movie?api_key=a5842037c731d7032cb2525fbd96ec26&language=fr-FR&query=' + this.state.searchInput  + '&page=1&include_adult=false')
        .then(res => res.json())
        .then(res => {
            console.log(res);
            this.props.callbackFromParent(res);
        });
    }

    /* handleSearch(e) {
        let currentList = [];
        let newList = [];

        if (e.target.value !== '') {
            currentList = this.props.items;

            newList = currentList.filter(item => {
                const lc = item.toLowerCase();
                const filter = e.target.value.toLowerCase();

                return lc.includes(filter);
            });
        } else {
            newList = this.props.items;
        }

        this.setState({
            filtered: newList
        });
    } */
}

export default SearchBar;