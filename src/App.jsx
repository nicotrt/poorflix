import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/header/header_component.jsx';
import Footer from './components/footer/footer_component.jsx';
import SearchBar from './components/searchBar/searchbar_component.jsx';
import MoviesList from './components/moviesList/moviesList_component.jsx';
import Favorites from './components/favList/fav_component.jsx';
import { Container, Row, Col } from 'react-bootstrap';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      arrayMovies : [],
      movieToAdd: null,
    }
    this.callbackArrayMovies = this.callbackArrayMovies.bind(this);
    this.callbackFavMovie = this.callbackFavMovie.bind(this);
  }

  callbackArrayMovies(movies) {
    this.setState({ arrayMovies: movies});
    console.log("data successfully fetched", movies);
  }

  callbackFavMovie(movie) {
    this.setState({movieToAdd: movie});
    console.log("movie successfully added to FavList", movie);
  }

  render() {
    return (
      <div className="App">
        <Header />
        <Container fluid={true}>
          <Row>
            <Col sm={3}>
              <Favorites movie={this.state.movieToAdd}/>
            </Col>
            <Col sm={9}>
              <SearchBar callbackFromParent={this.callbackArrayMovies} />
              <MoviesList data={this.state.arrayMovies.results} callbackFromParent={this.callbackFavMovie} />      
            </Col>
          </Row> 
        </Container>
        <Footer />
      </div>
    );
  }
}


export default App;
